Documenting Moore
=================

This documentation is build by `Sphinx`_. It was decided to use the `Google style guide`_ for writing docstrings in Python, which is illustrated in `the example`_.

Building the documentation locally
----------------------------------

When building the docs for the first time, the following lines of code should do the trick:

.. code-block:: sh

  . /cvmfs/lhcb.cern.ch/lib/LbEnv-testing.sh
  . /cvmfs/lhcbdev.cern.ch/nightlies/lhcb-head/Yesterday/setupSearchPath.sh
  $VIRTUAL_ENV/clone_venv moore_venv
  unset LBENV_SOURCED
  source moore_venv/bin/LbEnv.sh -r $MYSITEROOT
  pip install -r Moore/doc/requirements.txt
  pip install pydot
  pip install graphviz
  Moore/run env PYTHONHOME=$(pwd)/moore_venv make -C Moore/doc html


Once the new env is set up, it can be used again like this:

.. code-block:: sh

  . /cvmfs/lhcb.cern.ch/lib/LbEnv-testing.sh
  source moore_venv/bin/LbEnv.sh -r $MYSITEROOT
  Moore/run env PYTHONHOME=$(pwd)/moore_venv make -C Moore/doc html

.. _Sphinx: https://www.sphinx-doc.org/en/master/
.. _Google style guide: https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
.. _the example: https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html#example-google
