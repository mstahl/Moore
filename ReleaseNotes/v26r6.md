2017-09-13 Moore v26r6
========================================

Production release for 2017 data taking
----------------------------------------
Based on Gaudi v28r2, LHCb v42r6p1, Rec v21r6p1, Phys v23r6p1 and Hlt v26r6.

- Fix HltCache dependency, !102 (@rmatev)
- Improve tests and update PrChecker references, !100 (@rmatev)
- Propagate better the database tag defaults, !103 (@rmatev)
