###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Maker functions for Particle definitions common across HLT2.

The Run 2 code makes the sensible choice of creating Particle objects first,
and then filtering these with FilterDesktop instances. Because the
FunctionalParticleMaker can apply LoKi cut strings directly to Track and
ProtoParticle objects, we just do the one step.
"""
from __future__ import absolute_import, division, print_function

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from PyConf.Algorithms import FunctionalParticleMaker
from PyConf.Tools import (LoKi__Hybrid__ProtoParticleFilter as
                          ProtoParticleFilter, LoKi__Hybrid__TrackSelector as
                          TrackSelector)

from PyConf import configurable

from .algorithms import (require_all, ParticleFilter, ParticleFilterWithPVs,
                         ParticleCombiner, ParticleCombinerWithPVs,
                         PhotonFilter)
from .hacks import patched_hybrid_tool
from RecoConf.reco_objects_from_file import (
    make_protoparticles as _make_protoparticles, make_pvs as _make_pvs,
    make_neutral_protoparticles as _make_neutral_protoparticles)

_KAON0_M = 497.611 * MeV  # +/- 0.013, PDG, PR D98, 030001 and 2019 update
_LAMBDA_M = 1115.683 * MeV  # +/- 0.006, PDG, PR D98, 030001 and 2019 update


@configurable
def get_long_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrLONG', Code), **kwargs)


@configurable
def get_down_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrDOWNSTREAM', Code), **kwargs)


@configurable
def get_upstream_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrUPSTREAM', Code), **kwargs)


@configurable
def standard_protoparticle_filter(Code='PP_ALL', **kwargs):
    return ProtoParticleFilter(
        Code=Code, Factory=patched_hybrid_tool('PPFactory'), **kwargs)


@configurable
def make_particles(species='pion',
                   make_protoparticles=_make_protoparticles,
                   get_track_selector=get_long_track_selector,
                   make_protoparticle_filter=standard_protoparticle_filter):
    particles = FunctionalParticleMaker(
        InputProtoParticles=make_protoparticles(),
        ParticleID=species,
        TrackSelector=get_track_selector(),
        ProtoParticleFilter=make_protoparticle_filter())
    return particles


@configurable
def make_photons(species='Gamma',
                 make_neutral_protoparticles=_make_neutral_protoparticles,
                 **kwargs):
    return PhotonFilter(make_neutral_protoparticles(), species, **kwargs)


#Long particles
def make_long_electrons_no_brem():
    return make_particles(
        species="electron",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_pions():
    return make_particles(
        species="pion",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_kaons():
    return make_particles(
        species="kaon",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_protons():
    return make_particles(
        species="proton",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_muons():
    return make_particles(
        species="muon",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


#Down particles
def make_down_pions():
    return make_particles(
        species="pion",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_kaons():
    return make_particles(
        species="kaon",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_protons():
    return make_particles(
        species="proton",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


@configurable
def make_phi2kk(am_max=1100. * MeV, adoca_chi2=30, vchi2=25.0):
    kaons = make_long_kaons()
    descriptors = ['phi(1020) -> K+ K-']
    combination_code = require_all("AM < {am_max}",
                                   "ADOCACHI2CUT({adoca_chi2}, '')").format(
                                       am_max=am_max, adoca_chi2=adoca_chi2)
    vertex_code = "(VFASPF(VCHI2) < {vchi2})".format(vchi2=vchi2)
    return ParticleCombiner(
        particles=kaons,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_mass_constrained_jpsi2mumu(pid_mu=0,
                                    pt_mu=0.5 * GeV,
                                    admass=150. * MeV,
                                    adoca_chi2=20,
                                    vchi2=16):
    muons = make_long_muons()
    descriptors = ['J/psi(1S) -> mu+ mu-']
    daughters_code = {
        'mu+':
        '(PIDmu > {pid_mu}) & (PT > {pt_mu})'.format(
            pid_mu=pid_mu, pt_mu=pt_mu)
    }
    combination_code = require_all("ADAMASS('J/psi(1S)') < {admass}",
                                   "ADOCACHI2CUT({adoca_chi2}, '')").format(
                                       admass=admass, adoca_chi2=adoca_chi2)
    vertex_code = require_all("VFASPF(VCHI2) < {vchi2}",
                              "MFIT").format(vchi2=vchi2)
    return ParticleCombiner(
        particles=muons,
        DecayDescriptors=descriptors,
        DaughtersCuts=daughters_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


# Make V0s
def _make_long_for_V0(particles, pvs):
    code = require_all("BPVVALID()", "MIPCHI2DV(PRIMARY)>36")
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def _make_down_for_V0(particles):
    code = require_all("P>3000*MeV", "PT > 175.*MeV")
    return ParticleFilter(particles, Code=code)


def make_long_pions_for_V0():
    return _make_long_for_V0(make_long_pions(), _make_pvs())


def make_long_protons_for_V0():
    return _make_long_for_V0(make_long_protons(), _make_pvs())


def make_down_pions_for_V0():
    return _make_down_for_V0(make_down_pions())


def make_down_protons_for_V0():
    return _make_down_for_V0(make_down_protons())


@configurable
def _make_V0LL(particles,
               descriptors,
               pname,
               pvs,
               am_dmass=50 * MeV,
               m_dmass=35 * MeV,
               vchi2pdof_max=30,
               bpvltime_min=2.0 * picosecond):
    """Make long-long V0 -> h+ h'- candidates
    Initial implementation a replication of the old Hlt2SharedParticles
    """
    combination_code = require_all("ADAMASS('{pname}') < {am_dmass}").format(
        pname=pname, am_dmass=am_dmass)
    vertex_code = require_all("ADMASS('{pname}')<{m_dmass}",
                              "CHI2VXNDOF<{vchi2pdof_max}",
                              "BPVLTIME() > {bpvltime_min}").format(
                                  pname=pname,
                                  m_dmass=m_dmass,
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvltime_min=bpvltime_min)
    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def _make_V0DD(particles,
               descriptors,
               pvs,
               am_min=_KAON0_M - 80 * MeV,
               am_max=_KAON0_M + 80 * MeV,
               m_min=_KAON0_M - 64 * MeV,
               m_max=_KAON0_M + 64 * MeV,
               vchi2pdof_max=30,
               bpvvdz_min=400 * mm):
    """Make down-down V0 -> h+ h'- candidates
    Initial implementation a replication of the old Hlt2SharedParticles
    """
    combination_code = require_all("in_range({am_min},  AM, {am_max})").format(
        am_min=am_min, am_max=am_max)
    vertex_code = require_all("in_range({m_min},  M, {m_max})",
                              "CHI2VXNDOF<{vchi2pdof_max}",
                              "BPVVDZ() > {bpvvdz_min}").format(
                                  m_min=m_min,
                                  m_max=m_max,
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdz_min=bpvvdz_min)
    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def make_KsLL():
    pions = make_long_pions_for_V0()
    descriptors = ["KS0 -> pi+ pi-"]
    return _make_V0LL(
        particles=[pions],
        descriptors=descriptors,
        pname='KS0',
        pvs=_make_pvs())


def make_KsDD():
    pions = make_down_pions_for_V0()
    descriptors = ["KS0 -> pi+ pi-"]
    return _make_V0DD(
        particles=[pions], descriptors=descriptors, pvs=_make_pvs())


def make_LambdaLL():
    pions = make_long_pions_for_V0()
    protons = make_long_protons_for_V0()
    descriptors = ["[Lambda0 -> p+ pi-]cc"]
    return _make_V0LL(
        particles=[pions, protons],
        descriptors=descriptors,
        pname='Lambda0',
        pvs=_make_pvs(),
        am_dmass=50 * MeV,
        m_dmass=20 * MeV,
        vchi2pdof_max=30,
        bpvltime_min=2.0 * picosecond)


@configurable
def make_LambdaDD():
    pions = make_down_pions_for_V0()
    protons = make_down_protons_for_V0()
    descriptors = ["[Lambda0 -> p+ pi-]cc"]
    return _make_V0DD(
        particles=[pions, protons],
        descriptors=descriptors,
        pvs=_make_pvs(),
        am_min=_LAMBDA_M - 80 * MeV,
        am_max=_LAMBDA_M + 80 * MeV,
        m_min=_LAMBDA_M - 21 * MeV,
        m_max=_LAMBDA_M + 24 * MeV,
        vchi2pdof_max=30,
        bpvvdz_min=400 * mm)


# Make pions
@configurable
def make_has_rich_long_pions():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_long_pions()


@configurable
def make_has_rich_down_pions():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_down_pions()


# Make kaons
@configurable
def make_has_rich_long_kaons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_long_kaons()


@configurable
def make_has_rich_down_kaons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_down_kaons()


# Make protons
@configurable
def make_has_rich_long_protons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_long_protons()


@configurable
def make_has_rich_down_protons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_down_protons()


# Make muons
@configurable
def make_ismuon_long_muon():
    with standard_protoparticle_filter.bind(Code='PP_ISMUON'):
        return make_long_muons()
