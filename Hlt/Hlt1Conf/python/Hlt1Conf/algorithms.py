###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
from PyConf import configurable
from Configurables import (
    PrFilter__PrVeloTracks,
    PrFilter__PrFittedForwardTracksWithMuonID,
    PrFilter__PrFittedForwardTracksWithPVs,
    CombineTracks__2Body__Track_v2,
    CombineTracks__2Body__Track_v2__Dumper,
    CombineTracks__2Body__PrFittedForwardTracksWithPVs,
    CombineTracks__2Body__PrFittedForwardTracksWithPVs__Dumper,
    CombineTracks__2Body__PrFittedForwardTracksWithMuonID,
    CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper,
    Unwrap__PrFittedForwardTracksWithMuonID,
    Unwrap__PrFittedForwardTracksWithPVs,
)
from Functors import FILTER

from PyConf.components import Algorithm


def require_all(*cuts):
    """Return a cut string that requires each of the argument strings.

    Example usage:

        >>> require_all('M < 8*GeV', 'PT > 3*GeV')
        '((M < 8*GeV) & (PT > 3*GeV))'
    """
    return "({})".format(" & ".join("({})".format(cut) for cut in cuts))


def require_any(*cuts):
    """Return a cut string that requires at least one of the argument strings.

    Example usage:

        >>> require_any('M < 8*GeV', 'PT > 3*GeV')
        '((M < 8*GeV) | (PT > 3*GeV))'
    """
    return "({})".format(" | ".join("({})".format(cut) for cut in cuts))


def Filter(objects, functor):
    '''Return filters with the same output types as the inputs'''
    output = {}
    for key, alg_t, unwrap_t in [('PrFittedForwardWithPVs',
                                  PrFilter__PrFittedForwardTracksWithPVs,
                                  Unwrap__PrFittedForwardTracksWithPVs),
                                 ('PrFittedForwardWithMuonID',
                                  PrFilter__PrFittedForwardTracksWithMuonID,
                                  Unwrap__PrFittedForwardTracksWithMuonID),
                                 ('PrVeloTracks', PrFilter__PrVeloTracks,
                                  None)]:
        if key not in objects: continue
        filtered = Algorithm(
            alg_t, Input=objects[key], Cut=FILTER(functor)).Output
        if unwrap_t is not None:
            unwrapped = Algorithm(unwrap_t, Input=filtered).Output
            output['Scalar__' + key] = unwrapped
        output[key] = filtered
    return output


@configurable
def CombineTracks(NBodies=2,
                  CombinationCut=None,
                  VertexCut=None,
                  VoidDump=None,
                  ChildDump=None,
                  CombinationDump=None,
                  VertexDump=None,
                  PrTracks=False,
                  TracksWithMuonID=False,
                  **kwargs):
    """Return a configured CombineTracks instance.

    If any of `ChildDump`, `CombinationDump`, or `VertexDump` are not None, a
    version of the combiner is returned that will produce an ntuple. The ntuple
    has one row for each combination, and is useful for inspecting values that
    the combiner uses to evaluate selections. Each `Dump` argument is a
    dictionary whose keys are strings, corresponding to the row in the ntuple
    that will be filled with the functor value.

    Parameters
    ----------
    NBodies : int
        The number of tracks entering each combination. Currently only 2-track
        combinations are supported.
    CombinationCut : Functors.Functor
        Functor to be applied to the N-track combination object.
    VertexCut : Functors.Functor
        Functor to be applied to the vertex object.
    VoidDump : dict of str to Functors.Functor
        Functors to be evaluated for each combination when creating an ntuple.
    ChildDump : dict of str to Functors.Functor
        Functors to be evaluated for each child in each combination when
        creating an ntuple.
    CombinationDump : dict of str to Functors.Functor
        Functors to be evaluated for each combination when creating an ntuple.
    VertexDump : dict of str to Functors.Functor
        Functors to be evaluated for each combination when creating an ntuple.
    PrTracks : bool
        If True, a combiner algorithm instance that accepts
        PrFittedForwardTrack objects is returned. Otherwise, an instance that
        accepts Track::v2 objects is returned.
    """
    assert NBodies == 2

    def parse(input_dict):
        if input_dict is None: return {}
        return {
            k: [v.code(), v.code_repr()] + v.headers()
            for k, v in input_dict.items()
        }

    VoidDump_dict = parse(VoidDump)
    ChildDump_dict = parse(ChildDump)
    CombinationDump_dict = parse(CombinationDump)
    VertexDump_dict = parse(VertexDump)
    enable_dumper = any((len(ChildDump_dict), len(CombinationDump_dict),
                         len(VertexDump_dict), len(VoidDump_dict)))
    if enable_dumper:
        kwargs['VoidDump'] = VoidDump_dict
        kwargs['ChildDump'] = ChildDump_dict
        kwargs['VertexDump'] = VertexDump_dict
        kwargs['CombinationDump'] = CombinationDump_dict
    if PrTracks:
        if TracksWithMuonID:
            if enable_dumper:
                AlgType = CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper
            else:
                AlgType = CombineTracks__2Body__PrFittedForwardTracksWithMuonID
        else:
            if enable_dumper:
                AlgType = CombineTracks__2Body__PrFittedForwardTracksWithPVs__Dumper
            else:
                AlgType = CombineTracks__2Body__PrFittedForwardTracksWithPVs
    elif enable_dumper:
        AlgType = CombineTracks__2Body__Track_v2__Dumper
    else:
        AlgType = CombineTracks__2Body__Track_v2
    return Algorithm(
        AlgType, VertexCut=VertexCut, CombinationCut=CombinationCut, **kwargs)
