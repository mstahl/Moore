###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC B2DD lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import (prefilters,
                                                       upfront_reconstruction)

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

all_lines = {}

##############################################
# From the B2DD_Builder
##############################################


@register_line_builder(all_lines)
@configurable
def BdToDpDm_DpToHHH_Dm2HHH_line(name='Hlt2B2OC_BdToDpDm_DpToHHH_Dm2HHH_Line',
                                 prescale=1):
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(particles=[d], descriptors=['B0 -> D+ D-'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])
