###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC Lb2Lch lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import (prefilters,
                                                       upfront_reconstruction)

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import lc_builder
from Hlt2Conf.lines.b_to_open_charm.builders import lb_builder

all_lines = {}

##############################################
# Lb2Lch lines
##############################################


@register_line_builder(all_lines)
@configurable
def LbtoLcPi_LcToPKPi_line(name='Hlt2B2OC_LbToLcPi_LcToPKPi_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    lc = lc_builder.make_lc_to_pkpi()
    line_alg = lb_builder.make_lb2x(
        particles=[lc, bachelor],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])
