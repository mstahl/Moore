###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builds B2OC B decays, and defines the common default cuts applied to the B2X combinations
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond
from Hlt2Conf.algorithms import require_all, ParticleFilter, ParticleCombiner, ParticleCombinerWithPVs, ParticleFilterWithPVs
from Hlt2Conf.framework import configurable


@configurable
def make_b2x(
        particles,
        descriptors,
        name="B2OCB2XCombiner",
        am_min=5050 * MeV,
        am_max=5650 * MeV,
        am_min_vtx=5050 * MeV,
        am_max_vtx=5650 * MeV,
        sum_pt_min=5 * GeV,
        vtx_chi2pdof_max=20,  # was 10 in Run1+2
        bpvipchi2_max=25,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.999):
    '''
    Default B decay maker: defines default vtc chi2 and B mass range.
    '''
    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "APT > {sum_pt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       sum_pt_min=sum_pt_min)
    vertex_code = require_all("in_range({am_min_vtx}, M, {am_max_vtx})",
                              "VFASPF(VCHI2PDOF) < {vtx_chi2pdof_max}",
                              "BPVIPCHI2() < {bpvipchi2_max}",
                              "BPVLTIME() > {bpvltime_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  am_min_vtx=am_min_vtx,
                                  am_max_vtx=am_max_vtx,
                                  vtx_chi2pdof_max=vtx_chi2pdof_max,
                                  bpvipchi2_max=bpvipchi2_max,
                                  bpvltime_min=bpvltime_min,
                                  bpvdira_min=bpvdira_min)

    return ParticleCombiner(
        name=name,
        particles=particles,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)
