###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B2OC basic objects: pions, kaons, ...
"""
from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.reco_objects_from_file import make_pvs as get_pvs_from_file

from Hlt2Conf.algorithms import require_all, ParticleFilter, ParticleCombiner, ParticleCombinerWithPVs, ParticleFilterWithPVs
from Hlt2Conf.framework import configurable
from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions,
    make_has_rich_long_kaons,
    make_has_rich_long_protons,
    make_long_pions,
    make_down_pions,
)

####################################
# Track selections                 #
####################################


@configurable
def make_selected_particles(make_particles=make_has_rich_long_pions,
                            make_pvs=get_pvs_from_file,
                            trchi2_max=3,
                            mipchi2_min=4,
                            pt_min=250 * MeV,
                            p_min=2 * GeV,
                            pid=None):

    code = require_all(
        'PT > {pt_min}',
        'P > {p_min}',
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
        'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            mipchi2_min=mipchi2_min)
    if pid is not None:
        code += ' & ({})'.format(pid)
    return ParticleFilterWithPVs(make_particles(), make_pvs(), Code=code)


@configurable
def make_pions(pid='PIDK < 5'):
    """Return pions filtered by thresholds common to B2OC decay product selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_pions, pid=pid)


@configurable
def make_kaons(pid='PIDK > -5'):
    """Return kaons filtered by thresholds common to B2OC decay product selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_kaons, pid=pid)


@configurable
def make_protons(pid='PIDp > -5'):
    """Return protons filtered by thresholds common to B2OC decay product selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_protons, pid=pid)


@configurable
def make_bachelor_pions(pid='PIDK < 20', p_min=5 * GeV, pt_min=500 * MeV):
    """Return pions filtered by thresholds common to B2OC bachelor selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_pions,
        pid=pid,
        p_min=p_min,
        pt_min=pt_min)


@configurable
def make_bachelor_kaons(pid='PIDK > -10', p_min=5 * GeV, pt_min=500 * MeV):
    """Return kaons filtered by thresholds common to B2OC bachelor selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_kaons,
        pid=pid,
        p_min=p_min,
        pt_min=pt_min)


####################################
# 2-body decays                    #
####################################


@configurable
def make_ks(particles,
            name="B2OCKSBuilder",
            adamass=50. * MeV,
            adocachi2cut=30,
            pi_pmin=2 * GeV,
            pi_mipchi2pv=9.,
            admass=35. * MeV,
            chi2vx=30,
            bpvvdchi2=None):
    '''
    Build Kshort candidates. Default cuts correspond to VeryLooseKSLL from the
    Run2 CommonParticles
    '''
    descriptors = ['KS0 -> pi+ pi-']
    combination_code = require_all("ADAMASS('KS0') < {adamass}",
                                   "ADOCACHI2CUT({adocachi2cut}, '')").format(
                                       adamass=adamass,
                                       adocachi2cut=adocachi2cut)
    daughters_code = {
        "pi+":
        "(P > {pi_pmin}) & (MIPCHI2DV(PRIMARY) > {pi_mipchi2pv})".format(
            pi_pmin=pi_pmin, pi_mipchi2pv=pi_mipchi2pv)
    }
    vertex_code = require_all("ADMASS('KS0') < {admass}",
                              "CHI2VX < {chi2vx}").format(
                                  admass=admass, chi2vx=chi2vx)
    if bpvvdchi2 is not None:
        vertex_code += " & (BPVVDCHI2()>{})".format(bpvvdchi2)
    return ParticleCombiner(
        name=name,
        particles=particles,
        DecayDescriptors=descriptors,
        DaughtersCuts=daughters_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_ks_LL():
    '''
    Builds LL Kshorts, currently corresponding to the Run2
    StdVeryLooseKSLL. Hard-coded cuts.
    '''
    return make_ks(
        make_long_pions(),
        name="B2OCKSLLBuilder",
        adamass=50. * MeV,
        adocachi2cut=30,
        pi_pmin=2 * GeV,
        pi_mipchi2pv=9.,
        admass=35. * MeV,
        chi2vx=30,
        bpvvdchi2=4.)


@configurable
def make_ks_DD():
    '''
    Builds DD Kshorts, currently corresponding to the Run2
    StdLooseKSDD. Hard-coded cuts.
    '''
    return make_ks(
        make_down_pions(),
        name="B2OCKSDDBuilder",
        adamass=80. * MeV,
        adocachi2cut=25,
        pi_pmin=2 * GeV,
        pi_mipchi2pv=4.,
        admass=64. * MeV,
        chi2vx=25)


# TODO: LD K-shorts?
