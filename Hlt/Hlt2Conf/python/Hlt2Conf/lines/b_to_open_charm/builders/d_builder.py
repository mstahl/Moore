###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of detached D/D0/Ds/... meson decays that are shared between many
B2OC selections, and therefore are defined centrally.
"""
from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.reco_objects_from_file import make_pvs as get_pvs_from_file

from Hlt2Conf.algorithms import (
    require_all, ParticleFilter, ParticleCombiner, ParticleCombinerWithPVs,
    ParticleFilterWithPVs, N3BodyCombinerWithPVs, N4BodyCombinerWithPVs)
from Hlt2Conf.framework import configurable
from Hlt2Conf.standard_particles import make_has_rich_long_pions, make_has_rich_long_kaons

from . import basic_builder

###############################################################################
# Generic D decay builders, defines default combination cuts                  #
###############################################################################


@configurable
def make_d_to_twobody(particles,
                      descriptors,
                      name="B2OCD2twobodyCombiner",
                      make_pvs=get_pvs_from_file,
                      asumpt_min=1800 * MeV,
                      am_min=1764.84 * MeV,
                      am_max=1964.84 * MeV,
                      adoca12_min=0.5 * mm,
                      vchi2pdof_max=10,
                      bpvvdchi2_min=36,
                      bpvdira_min=0):
    """
    A generic D->2body decay maker

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "ASUM(PT) > {asumpt_min}",
                                   "ADOCA(1,2) < {adoca12_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       asumpt_min=asumpt_min,
                                       adoca12_max=adoca12_min)
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvdira_min=bpvdira_min)
    return ParticleCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_d_to_threebody(particles,
                        descriptors,
                        name="B2OCD2threebodyCombiner",
                        make_pvs=get_pvs_from_file,
                        adoca12_max=0.5 * mm,
                        asumpt_min=1800 * MeV,
                        am_min=1789 * MeV,
                        am_max=1949 * MeV,
                        adoca13_max=0.5 * mm,
                        adoca23_max=0.5 * mm,
                        vchi2pdof_max=10,
                        bpvvdchi2_min=36,
                        bpvdira_min=0):
    """
    A generic D->3body decay maker. Makes use of N3BodyCombinerWithPVs
    to be more efficient, first making a DOCAcut on the *2 first particles in the
    decay descriptor*.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination12_code = require_all("ADOCA(1,2) < {adoca12_max}").format(
        adoca12_max=adoca12_max)
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "ASUM(PT) > {asumpt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       asumpt_min=asumpt_min)
    if adoca13_max is not None:
        combination_code = require_all(
            combination_code,
            "ADOCA(1,3) < {adoca13_max}").format(adoca13_max=adoca13_max)
    if adoca23_max is not None:
        combination_code = require_all(
            combination_code,
            "ADOCA(2,3) < {adoca23_max}").format(adoca23_max=adoca23_max)
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvdira_min=bpvdira_min)
    return N3BodyCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


###############################################################################
# Specific D decay builders, overrides default cuts where needed              #
###############################################################################


@configurable
def make_dzero_to_hh(**decay_arguments):
    """
    Defines the default B2OC D0->h+h- decay

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the D->3body
                        maker
    """
    particles = [basic_builder.make_kaons(), basic_builder.make_pions()]
    descriptors = [
        'D0 -> pi+ pi-', 'D0 -> K+ K-', 'D0 -> pi+ K-', 'D0 -> K+ pi-'
    ]
    return make_d_to_twobody(
        particles, descriptors, name="B2OCD02HHCombiner", **decay_arguments)


@configurable
def make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL(), **decay_arguments):
    """
    Defines the default B2OC D->Ks hh decay

    Parameters
    ----------
    k_shorts :
      selected KS candidate maker (typically choose LL or DD)
    **decay_arguments : keyword arguments are passed on to the D->3body
                        maker
    """
    descriptors = [
        'D0 -> pi+ pi- KS0', 'D0 -> K+ K- KS0', 'D0 -> pi+ K- KS0',
        'D0 -> K+ pi- KS0'
    ]
    particles = [
        k_shorts,
        basic_builder.make_kaons(),
        basic_builder.make_pions()
    ]
    return make_d_to_threebody(
        particles,
        descriptors,
        name="B2OCD02KSHHCombiner",
        adoca13_max=None,
        adoca23_max=None,
        **decay_arguments)


@configurable
def make_dplus_to_hhh(**decay_arguments):
    """
    Return a D+ -> h+h-h+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the D->3body
                        maker
    """
    particles = [basic_builder.make_kaons(), basic_builder.make_pions()]
    descriptors = [
        '[D+ -> pi+ pi+ pi-]cc', '[D+ -> pi+ pi+ K-]cc',
        '[D+ -> pi+ K+ pi-]cc', '[D+ -> pi+ K+ K-]cc', '[D+ -> K+ K+ pi-]cc',
        '[D+ -> K+ K+ K-]cc'
    ]
    return make_d_to_threebody(
        particles, descriptors, name="B2OCD2HHHCombiner", **decay_arguments)


@configurable
def make_dsplus_to_hhh(**decay_arguments):
    """
    Return a D_s+ -> h+h-h+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the D->3body
                        maker
    """
    particles = [basic_builder.make_kaons(), basic_builder.make_pions()]
    descriptors = [
        '[D_s+ -> pi+ pi+ pi-]cc',
        '[D_s+ -> pi+ pi+ K-]cc',
        '[D_s+ -> pi+ K+ pi-]cc',
        '[D_s+ -> pi+ K+ K-]cc',
        '[D_s+ -> K+ K+ pi-]cc',
        '[D_s+ -> K+ K+ K-]cc',
    ]
    return make_d_to_threebody(
        particles, descriptors, name="B2OCDS2HHHCombiner", **decay_arguments)
