Control flow
============

TODO: describe what the control flow and one can construct it.

.. autoclass:: PyConf.control_flow.NodeLogic
   :members:

.. autoclass:: PyConf.control_flow.CompositeNode
   :members:
