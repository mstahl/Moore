###############################################################################
# (c) Copyright To019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDh lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import (prefilters,
                                                       upfront_reconstruction)

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

all_lines = {}

##############################################
# From the BToDh_Builder
##############################################


@register_line_builder(all_lines)
@configurable
def BdToDmK_DmToHHH_line(name='Hlt2B2OC_BdToDmK_DmToHHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['[B0 -> D- K+]cc'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDmPi_DmToHHH_line(name='Hlt2B2OC_BdToDmPi_DmToHHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['[B0 -> D- pi+]cc'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


##############################################
# GLW/ADS lines
##############################################


@register_line_builder(all_lines)
@configurable
def BuToD0K_D0ToHH_line(name='Hlt2B2OC_BuToD0K_D0ToHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_D0ToHH_line(name='Hlt2B2OC_BuToD0Pi_D0ToHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


##############################################
# GGSZ lines
##############################################


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_D0ToKsLLHH_line(name='Hlt2B2OC_BuToD0Pi_D0ToKsLLHH_Line',
                             prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_D0ToKsDDHH_line(name='Hlt2B2OC_BuToD0Pi_D0ToKsDDHH_Line',
                             prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0K_D0ToKsLLHH_line(name='Hlt2B2OC_BuToD0K_D0ToKsLLHH_Line',
                            prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0K_D0ToKsDDHH_line(name='Hlt2B2OC_BuToD0K_D0ToKsDDHH_Line',
                            prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


##############################################
# GGSZ lines for part reco D decays
##############################################
# Lines for partially reconstructed GGSZ lines with wider mass windows
# but (still to be implemented) tighter cuts


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_PartialD0ToKsLLHH_line(
        name='Hlt2B2OC_BuToD0Pi_PartialD0ToKsLLHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), am_min=1000 * MeV)
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        am_min=4500 * MeV)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_PartialD0ToKsDDHH_line(
        name='Hlt2B2OC_BuToD0Pi_PartialD0ToKsDDHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), am_min=1000 * MeV)
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        am_min=4500 * MeV)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0K_PartialD0ToKsLLHH_line(
        name='Hlt2B2OC_BuToD0K_PartialD0ToKsLLHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), am_min=1000 * MeV)
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        am_min=4500 * MeV)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0K_PartialD0ToKsDDHH_line(
        name='Hlt2B2OC_BuToD0K_PartialD0ToKsDDHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), am_min=1000 * MeV)
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        am_min=4500 * MeV)
    return HltLine(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + prefilters() + [line_alg])
